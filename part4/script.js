/*
// === === === === === === === === === === === === === ===
//                  Keyed collections
// === === === === === === === === === === === === === ===
// Map object 
// مپ آبجکت مجموعه ای از المان ها هستن که مقادیرشون در جفت کلید ولیو ذخیره میشن
//ک Map، لیستی بی نظم از جفت‌های مقادیر کلیدی است که در آن‌ها،‌ کلید و مقدار می‌توانند از هر نوعی باشند.

var map1 = new Map([1, 2], [2, 3], [4, 5]);
console.log(map1);
*/

// let saying = new Map();
// saying.set('asb', 'pitiko'); // ست کردن جفت المان
// saying.set('khar', 'arar');
// saying.set('goorbe', 'miew');
// console.log(saying);
// Map(3) { "asb" => "pitiko", "khar" => "arar", "goorbe" => "miew" }
//     [
//         [Entries]
//     ]
// 0: { "asb" => "pitiko" }
// 1: { "khar" => "arar" }
// 2: { "goorbe" => "miew" }
// size: (...)
// __proto__: Map

// console.log(saying.size); // سایز یک مپ  3saying.get('asb')

// "pitiko"
// saying.get('boz')  // متد گرفتن مقادیر از مپ 
// undefined
// saying.get('miew')
// undefined
// saying.get("khar")
// "arar"saying.has(asb)
// VM700: 1 Uncaught ReferenceError: asb is not defined
// at < anonymous >: 1: 12(anonymous) @ VM700: 1
// saying.has('asb')
// true
// saying.has('moosh')
// false

// Map(3)  { "asb" => "pitiko", "khar" => "arar", "goorbe" => "miew" }
// saying.delete('khar')
// true
// saying
// Map(2)  { "asb" => "pitiko", "goorbe" => "miew" }saying

// Map(2)  { "asb" => "pitiko", "goorbe" => "miew" }
// for (let [key, value] of saying) {   // پیمایش روی مپ 
//     console.log(`${key} sedaye ${value} midahad`);
// }
// VM1137: 2 asb sedaye pitiko midahad
// VM1137: 2 goorbe sedaye miew midahad
// undefined

// saying.set('sag', 'vagh vagh');
// Map(3)  { "asb" => "pitiko", "goorbe" => "miew", "sag" => "vagh vagh" }
// map.clear();
// VM1278: 1 Uncaught ReferenceError: map is not defined
// at < anonymous >: 1: 1(anonymous) @ VM1278: 1
// saying.clear
// ƒ clear() {
//     [native code]
// }
// saying.clear()   // خالی کردن عناصر مپ 
// undefined
// saying
// Map(0)  {}


// مقایسه Object and Map compared
// در آبجکت کلید فقط رشته یا عدد است اما در مپ هر چیزی میتونه باشه
// سایز مپ به راحتی قابل دسترسی است
// پیمایش در مپ ها به ترتیب درج عناصر استlet myMap = new Map();


// let myMap = new Map();

// myMap.set(1, 'Apple');←
// Map { 1 => "Apple" }

// myMap.set(1, 'Apple').set(2, 'Orange').set(3, 'Banana');←
// Map { 1 => "Apple", 2 => "Orange", 3 => "Banana" }

// myMap.set(1, 'Cherry');←
// Map { 1 => "Cherry", 2 => "Orange", 3 => "Banana" }
// myMap.set(4, "Orange");←
// Map { 1 => "Cherry", 2 => "Orange", 3 => "Banana", 4 => "Orange" }

// let myMap = new Map();
// myMap.set(1, 'Car').set('weight', 1000).set('color', 'Blue');←
// Map { 1 => "Car", "weight" => 1000, "color" => "Blue" }

// let person = new Map([
//     ['firstName', 'Abbas'],
//     ['lastName', 'Moqaddam'],
//     ['age', 33]
// ]);
// person;←
// Map { "firstName" => "Abbas", "lastName" => "Moqaddam", "age" => 33 }

// let person = new Map();
// person.set('firstName', 'Abbas').set('lastName', 'Moqaddam').set('age', 33);←
// Map { "firstName" => "Abbas", "lastName" => "Moqaddam", "age" => 33 }
// person.delete('age');←
// true
// person;←
// Map { "firstName" => "Abbas", "lastName" => "Moqaddam" }
// person.delete('weight');←
// false
// person.clear();
// person;←
// Map {}
// /----------------------------------------------
//                  مجموعه ها (Sets)
// /----------------------------------------------

// تفاوت مهم بین مجموعه‌ها و آرایه‌ها در این است که در یک آرایه می‌توان از عناصری با مقدار یکسان یا تکراری استفاده کرد. ولی در مجموعه‌ها هر مقدار فقط یک بار می‌تواند در مجموعه وجود داشته باشد و امکان تکرار وجود ندارد.
//let list = new Set();  // ساخت یک 
/*
list.add(1);←
Set { 1 }

list.add(2).add(3).add(4);←
Set { 1, 2, 3, 4 }

list.add(1);←
Set { 1, 2, 3, 4 }

let list = new Set([1, 2, 2, 3, 3, 4]);
list;←
Set { 1, 2, 3, 4 }

let list = new Set('Hello');
list;←
Set { "H", "e", "l", "o" }

let list = new Set(['Apple', 'Orange', 'Banana']);
list;←
Set { "Apple", "Orange", "Banana" }

let list = new Set(['2', 2]);
list;←
Set { "2", 2 }

let list = new Set(['Apple', 'Orange', 'Banana']);
list.size;←
3

let list = new Set(['Apple', 'Orange', 'Banana']);
list.has('Apple');←
true
list.has('Cherry');←
false

let list = new Set(['Apple', 'Orange', 'Banana']);
list.delete('Apple');←
true
list.delete('Cherry');←
false

let list = new Set(['Apple', 'Orange', 'Banana']);
list.clear();
list;←
Set {}
list.size;←

*/

//-----------------------------------------------
// object
//-----------------------------------------------
// let fruits = ['Apple', 'Orange', 'Banana', 'Cherry']; // آرایه نوعی از اشیا است

// let obj1 = new Object(); // روش اول
// let obj2 = {}; // روش دوم

// obj1.property1 = 10; // افزودن خاصیت جدید
// obj1.property1 = 20; // آپدیت خاصیت قبلی
// obj1.method1 = function() { // افزودن متد
//     console.log('I am a method');
// };

// obj1.method1 = function() { // ایجاد آبجکت با استفاده از Literal

//     console.log('I am a method');
// };

// var person = {
//     firstName: "John",
//     lastName: "Doe",
//     age: 50,
//     eyeColor: "blue"
// };


// var person = new Object(); // با استفاده از کلمه نیو
// person.firstName = "John";
// person.lastName = "Doe";
// person.age = 50;
// person.eyeColor = "blue";

// === === === === === === === === === === === === === ===
// bom
// === === === === === === === === === === === === === ===

// BOM یا Browser Object Model  
// برای ارتباط با مرورگر
// در محیط مرورگر شئ سراسری window
//BOM یا Browser Object Model یک مدل انتزاعی برای برقراری ارتباط بین مرورگر و جاوا اسکریپت است. در این مدل، مرورگر به عنوان یک شئ جاوا اسکریپت مدل‌سازی می‌شود که از طریق این شئ (همان شئ window) می‌توان به بسیاری از امکانات مرورگر دسترسی داشت.

// window.console.log('salam donya');
// window.alert('hello');
/*
window.outerHeight
728
window.outerHeight
706
window.outerWidth
1140
window.outerWidth
1283
window.innerHeight
150
window.innerHeight
201
window.innerHeight
201
window.innerHeight
235

let y = window.scrollY;
console.log();

window.location
Location {ancestorOrigins: DOMStringList, href: "file:///D:/aa%20taban%20shahr/tasks/js/JSpart4/New%20folder/index.html", origin: "file://", protocol: "file:", host: "", …}ancestorOrigins: DOMStringList {length: 0}assign: ƒ assign()hash: ""host: ""hostname: ""href: "file:///D:/aa%20taban%20shahr/tasks/js/JSpart4/New%20folder/index.html"origin: "file://"pathname: "/D:/aa%20taban%20shahr/tasks/js/JSpart4/New%20folder/index.html"port: ""protocol: "file:"reload: ƒ reload()replace: ƒ replace()search: ""toString: ƒ toString()valueOf: ƒ valueOf()Symbol(Symbol.toPrimitive): undefined__proto__: Location
window.location.host
""
window.location.port
""
window.location.file
undefined
window.location.href
"file:///D:/aa%20taban%20shahr/tasks/js/JSpart4/New%20folder/index.html"

window.location.search
"?id=2"
window.location.href="https://www.yahoo.com"; // ریدایرکت میشه به مسیری که میخوایم

window.location.reload() // ریلود شدن صفحه


window.history
History {length: 4, scrollRestoration: "auto", state: null}length: 4scrollRestoration: "auto"state: null__proto__: History

window.history.length
4

window.navigator
Navigator {vendorSub: "", productSub: "20030107", vendor: "Google Inc.", maxTouchPoints: 0, userActivation: UserActivation, …}appCodeName: "Mozilla"appName: "Netscape"appVersion: "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"connection: NetworkInformation {onchange: null, effectiveType: "4g", rtt: 250, downlink: 3.6, saveData: false}cookieEnabled: truedoNotTrack: nullgeolocation: Geolocation {}hardwareConcurrency: 8language: "en-US"languages: (3) ["en-US", "en", "fa"]maxTouchPoints: 0mediaCapabilities: MediaCapabilities {}mediaSession: MediaSession {metadata: null, playbackState: "none"}mimeTypes: MimeTypeArray {0: MimeType, 1: MimeType, 2: MimeType, 3: MimeType, application/pdf: MimeType, application/x-google-chrome-pdf: MimeType, application/x-nacl: MimeType, application/x-pnacl: MimeType, length: 4}onLine: truepermissions: Permissions {}platform: "Win32"plugins: PluginArray {0: Plugin, 1: Plugin, 2: Plugin, Chrome PDF Plugin: Plugin, Chrome PDF Viewer: Plugin, Native Client: Plugin, length: 3}product: "Gecko"productSub: "20030107"scheduling: Scheduling {}userActivation: UserActivation {hasBeenActive: true, isActive: true}userAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"vendor: "Google Inc."vendorSub: ""webdriver: falsewebkitPersistentStorage: DeprecatedStorageQuota {}webkitTemporaryStorage: DeprecatedStorageQuota {}__proto__: Navigator




/*
///////////////////////////////////////
// Constructor Functions and the new Operator   // متد سازنده 
const Person = function (firstName, birthYear) {
  // Instance properties
  this.firstName = firstName;
  this.birthYear = birthYear;

  // Never to this!
  // this.calcAge = function () {
  //   console.log(2037 - this.birthYear);
  // };
};

const Farzad = new Person('Farzad', 1991);
console.log(Farzad);

// 1. New {} is created
// 2. function is called, this = {}
// 3. {} linked to prototype
// 4. function automatically return {}

const mahsa = new Person('mahsa', 2017);
const jack = new Person('Jack', 1975);

console.log(Farzad instanceof Person);

Person.hey = function () {
  console.log('Hey there 👋');
  console.log(this);
};
Person.hey();

///////////////////////////////////////
// Prototypes
console.log(Person.prototype);

Person.prototype.calcAge = function () {
  console.log(2037 - this.birthYear);
};

Farzad.calcAge();
mahsa.calcAge();

console.log(Farzad.__proto__);
console.log(Farzad.__proto__ === Person.prototype);

console.log(Person.prototype.isPrototypeOf(Farzad));
console.log(Person.prototype.isPrototypeOf(mahsa));
console.log(Person.prototype.isPrototypeOf(Person));

// .prototyeOfLinkedObjects

Person.prototype.species = 'Homo Sapiens';
console.log(Farzad.species, mahsa.species);

console.log(Farzad.hasOwnProperty('firstName'));
console.log(Farzad.hasOwnProperty('species'));


///////////////////////////////////////
// Prototypal Inheritance on Built-In Objects
console.log(Farzad.__proto__);
// Object.prototype (top of prototype chain)
console.log(Farzad.__proto__.__proto__);
console.log(Farzad.__proto__.__proto__.__proto__);

console.dir(Person.prototype.constructor);

const arr = [3, 6, 6, 5, 6, 9, 9]; // new Array === []  // از هر دو روش میشه استفاده کرد
console.log(arr.__proto__);
console.log(arr.__proto__ === Array.prototype);

console.log(arr.__proto__.__proto__);

Array.prototype.unique = function () {
  return [...new Set(this)];
};

console.log(arr.unique());

const h1 = document.querySelector('h1');
console.dir(x => x + 1);
*/

/*
///////////////////////////////////////
// ES6 Classes   

// Class expression
// const PersonCl = class {}

// Class declaration
class PersonCl {
    constructor(fullName, birthYear) {
        this.fullName = fullName;
        this.birthYear = birthYear;
    }

    // Instance methods
    // Methods will be added to .prototype property
    calcAge() {
        console.log(2037 - this.birthYear);
    }

    greet() {
        console.log(`Hey ${this.fullName}`);
    }

    get age() {
        return 2037 - this.birthYear;
    }

    // Set a property that already exists
    set fullName(name) {
        if (name.includes(' ')) this._fullName = name;
        else alert(`${name} is not a full name!`);
    }

    get fullName() {
        return this._fullName;
    }

    // Static method
    static hey() {
        console.log('من اینجام');
        console.log(this);
    }
}

const leila = new PersonCl('leila Davis', 1996);
console.log(leila);
leila.calcAge();
console.log(leila.age);

console.log(leila.__proto__ === PersonCl.prototype);

// PersonCl.prototype.greet = function () {
//   console.log(`Hey ${this.firstName}`);
// };
leila.greet();

// 1. Classes are NOT hoisted
// 2. Classes are first-class citizens // اولویت
// 3. Classes are executed in strict mode  // کلاس ها توی استریکت مود اجرا میشن

const vahid = new PersonCl('vahid White', 1965);
// PersonCl.hey();


///////////////////////////////////////
// Setters and Getters
const account = {
    owner: 'Farzad',
    movements: [200, 530, 120, 300],

    get latest() {
        return this.movements.slice(-1).pop();
    },

    set latest(mov) {
        this.movements.push(mov);
    },
};

console.log(account.latest);

account.latest = 50;
console.log(account.movements);


///////////////////////////////////////
// Object.create
const PersonProto = {
    calcAge() {
        console.log(2037 - this.birthYear);
    },

    init(firstName, birthYear) {
        this.firstName = firstName;
        this.birthYear = birthYear;
    },
};

const salar = Object.create(PersonProto);
console.log(salar);
salar.name = 'salar';
salar.birthYear = 2002;
salar.calcAge();

console.log(salar.__proto__ === PersonProto);

const sarah = Object.create(PersonProto);
sarah.init('Sarah', 1979);
sarah.calcAge(); *
/

///////////////////////////////////////


///////////////////////////////////////
// Inheritance Between "Classes": ES6 Classes

class PersonCl {
  constructor(fullName, birthYear) {
    this.fullName = fullName;
    this.birthYear = birthYear;
  }

  // Instance methods
  calcAge() {
    console.log(2037 - this.birthYear);
  }

  greet() {
    console.log(`Hey ${this.fullName}`);
  }

  get age() {
    return 2037 - this.birthYear;
  }

  set fullName(name) {
    if (name.includes(' ')) this._fullName = name;
    else alert(`${name} is not a full name!`);
  }

  get fullName() {
    return this._fullName;
  }

  // Static method
  static hey() {
    console.log('Hey there 👋');
  }
}

class StudentCl extends PersonCl {
  constructor(fullName, birthYear, course) {
    // Always needs to happen first!
    super(fullName, birthYear);
    this.course = course;
  }

  introduce() {
    console.log(`My name is ${this.fullName} and I study ${this.course}`);
  }

  calcAge() {
    console.log(
      `I'm ${
        2037 - this.birthYear
      } years old, but as a student I feel more like ${
        2037 - this.birthYear + 10
      }`
    );
  }
}

const martha = new StudentCl('Martha Jones', 2012, 'Computer Science');
martha.introduce();
martha.calcAge();


///////////////////////////////////////
// Inheritance Between "Classes": Object.create

const PersonProto = {
  calcAge() {
    console.log(2037 - this.birthYear);
  },

  init(firstName, birthYear) {
    this.firstName = firstName;
    this.birthYear = birthYear;
  },
};

const salar = Object.create(PersonProto);

const StudentProto = Object.create(PersonProto);
StudentProto.init = function (firstName, birthYear, course) {
  PersonProto.init.call(this, firstName, birthYear);
  this.course = course;
};

StudentProto.introduce = function () {

  // console.log(`My name is ${this.fullName} and I study ${this.course}`);
  
  // FIX:
  console.log(`My name is ${this.firstName} and I study ${this.course}`);
};

const jafar = Object.create(StudentProto);
jafar.init('jafar', 2010, 'Computer Science');
jafar.introduce();
jafar.calcAge();


///////////////////////////////////////
// Encapsulation: Protected Properties and Methods // کپسوله سازی 
// Encapsulation: Private Class Fields and Methods

// 1) Public fields
// 2) Private fields
// 3) Public methods
// 4) Private methods
// (there is also the static version)

class Account {
  // 1) Public fields (instances)
  locale = navigator.language;

  // 2) Private fields (instances)
  #movements = [];
  #pin;

  constructor(owner, currency, pin) {
    this.owner = owner;
    this.currency = currency;
    this.#pin = pin;

    // Protected property
    // this._movements = [];
    // this.locale = navigator.language;

    console.log(`Thanks for opening an account, ${owner}`);
  }

  // 3) Public methods

  // Public interface
  getMovements() {
    return this.#movements;
  }

  deposit(val) {
    this.#movements.push(val);
    return this;
  }

  withdraw(val) {
    this.deposit(-val);
    return this;
  }

  requestLoan(val) {
    // if (this.#approveLoan(val)) {
    if (this._approveLoan(val)) {
      this.deposit(val);
      console.log(`Loan approved`);
      return this;
    }
  }

  static helper() {
    console.log('Helper');
  }

  // 4) Private methods
  // #approveLoan(val) {
  _approveLoan(val) {
    return true;
  }
}

const acc1 = new Account('Farzad', 'EUR', 1111);

// acc1._movements.push(250);
// acc1._movements.push(-140);
// acc1.approveLoan(1000);

acc1.deposit(250);
acc1.withdraw(140);
acc1.requestLoan(1000);
console.log(acc1.getMovements());
console.log(acc1);
Account.helper();

// console.log(acc1.#movements);
// console.log(acc1.#pin);
// console.log(acc1.#approveLoan(100));

// Chaining
acc1.deposit(300).deposit(500).withdraw(35).requestLoan(25000).withdraw(4000);
console.log(acc1.getMovements());
*/

///////////////////////////////////////