/*
//------------------------------------------
// باز کردن کنسول در مرورگر با اف 12
// alert('salam Tabane Shar'); // نمایش پیغام به کاربر
//------------------------------------------

////////////////////////////////////
// Linking a JavaScript File => <script src = # ><script>
let js = "amazing";
if (js == 'amazing') alert('با نام و یاد خدا شروع می کنیم 🤵');
console.log(40 + 8 + 23 - 10);

////////////////////////////////////
// Values and Variables
console.log("Hasan"); // نمایش در کنسول مرورگر
console.log(23);

let firstName = "Mahsa";

console.log(firstName); // mahsa 
console.log(firstName);
console.log(firstName);
//------------------------------------------
//------------------------------------------

// Variable name conventions
let 76 Gholam = true; //  نامگذاری غلط برای متغیر
let Sage_velgard = "JM";
let $function = 27; // غلط

let person = "ali";
let PI = 3.1415;

let myFirstJob = "Coder";
let myCurrentJob = "jigar foorosh";

let job1 = "programmer";
let job2 = "jigarForoosh";

console.log(myFirstJob);


////////////////////////////////////

// Data Types
let javascriptIsFun = true;
console.log(javascriptIsFun);

// console.log(typeof true);
console.log(typeof javascriptIsFun);
// console.log(typeof 23);
// console.log(typeof 'hamidreza');

javascriptIsFun = 'YES!';
console.log(typeof javascriptIsFun);

let year;
console.log(year);
console.log(typeof year);

year = 1400;
console.log(typeof year);

console.log(typeof null); // نوع نال  یک آبجکت است

let js = true;
undefined
console.log(js);
VM115:1 true
undefined
typeof js;
"boolean"
let PI = 3.24;
undefined
typeof PI;
"number"
typeof null;
"object"


////////////////////////////////////
/*  JavaScript Arrays

var cars = ["Saab", "Volvo", "BMW"];
var array_name = [item1, item2, ...]; 
var cars = new Array("Saab", "Volvo", "BMW");
var name = cars[0];
var cars = ["Saab", "Volvo", "BMW"];
document.getElementById("demo").innerHTML = cars[0]; 

var cars = ["Saab", "Volvo", "BMW"];
cars[0] = "Opel";
document.getElementById("demo").innerHTML = cars[0];

var cars = ["Saab", "Volvo", "BMW"];
document.getElementById("demo").innerHTML = cars;



/*
////////////////////////////////////


////////////////////////////////////
// let, const and var  // تفاوت بین اینها 
// var
//     Declares a variable, optionally initializing it to a value.
// let
// Declares a block - scoped, local variable, optionally initializing it to a value.
// const
// Declares a block - scoped, read - only named constant.


let age = 30;
age = 31;

const birthYear = 1995;
// birthYear = 1990;
// const job;

var job = 'programmer';
job = 'jigarForoosh'

lastName = 'rezaee';
console.log(lastName);
let a = 7;
undefined
a = 90;
90
a
90
const u = 'sahar';
undefined
u
    "sahar"
u = 6;
VM452: 1 Uncaught TypeError: Assignment to constant variable.
at < anonymous >: 1: 3


////////////////////////////////////
// Basic Operators
// Math operators


const now = 1400;
const ageFarzad = now - 1375;
const ageMohammad = now - 1380;
console.log(ageFarzad, ageMohammad); // 25 20

const firstName = 'ali';
const lastName = 'Jafari';
console.log(firstName + lastName); // aliJafari

const firstName = 'ali';
const lastName = 'Jafari';
console.log(firstName + ' ' + lastName); // ali Jafari



console.log(ageFArzad * 2, ageFarzad / 10, 2 ** 3);
// 2 ** 3 = 2 * 2 * 2



// Assignment operators
let x = 8;
x += 78;
console.log(x); // 86

let x = 10 + 5; // 15
x += 10; // x = x + 10 = 25
x *= 4; // x = x * 4 = 100
x++; // x = x + 1
x--;
x--;
console.log(x);


// Comparison operators
const x = 89;
const y = 900;
console.log(x > y); // >, <, >=, <=
console.log(y >= 18); // true



////////////////////////////////////
// Operator Precedence  // اولیت


const now = 2037;
const ageJavad = now - 1995;
const agemahsa = now - 2018;

console.log(now - 1995 > now - 2018);

let x, y;
x = y = 25 - 10 - 5; // x = y = 10, x = 10
console.log(x, y);

const averageAge = (ageFarzad + agemahsa) / 2;
console.log(ageFarzad, agemahsa, averageAge);

////////////////////////////////////

// Strings and Template Literals
const firstName = 'Farzad';
const job = 'jigarForoosh';
const birthYear = 1995;
const year = 2037;

const Farzad = "I'm " + firstName + ',a ' + job + ' and' + (year - birthYear) + ' years old';
// console.log(Farzad); //I'm Farzad,a jigarForoosh and46 years old 

const FarzadNew = `I'm ${firstName} ,a ${job} and ${(year - birthYear)} years old`;
console.log(FarzadNew); // I'm Farzad ,a jigarForoosh and 46 years old

console.log('man yek neveshte hastam mikham beram sare khat \n hala khate dovom hastam');

console.log(` man farzad hastam va
inja khate dovom ast`);
// با استفاده از بک تیکس که بالای تب هست بدون نیاز به استفاده ازاسلش ان میتونی بری سر خط // special characters
*
/

/*

////////////////////////////////////
// Taking Decisions: if / else Statements // ساختار شرطی ایف الس
const age = 15;

if (age >= 18) {
    console.log('mahsa can start driving license 🚗');
} else {
    const yearsLeft = 18 - age;
    console.log(`mahsa is too young. Wait another ${yearsLeft} years :)`);
}

const birthYear = 2012;

let century; // چون میخوایم درون شرط ازش استفاده کنیم باید قبلش تعریف کنیم
if (birthYear <= 2000) {
    century = 20;
} else {
    century = 21;
}
console.log(century); *
/

////////////////////////////////////
// Type Conversion and Coercion

// type conversion
const inputYear = '1995';
console.log(Number(inputYear), inputYear);
console.log(Number(inputYear) + 18);

console.log(Number('Farzad'));
console.log(typeof NaN);

console.log(String(23), 23);

// type coercion
console.log('I am ' + 23 + ' years old'); // جاوا اسکریپت خودکار عدد 23 رو به رشته تبدیل می کند
console.log('I am ' + String(23) + ' years old'); //  مثل خط بالا 
console.log('23' - '10' - 3); // تبدیل به عدد
console.log('23' / '2'); // تبدیل به عدد
console.log('23' > '18'); // تبدیل به عدد 
// فقط در + تبدیل عدد به رشته انجام میشه و در بقیه اپراتور ها رشته به عدد تبدیل میشه

let n = '1' + 1; // '11'
n = n - 1; // ان تبدیل به عدد 11 میشه و بعد یکی ازش کم میشه
console.log(n); // 10

////////////////////////////////////
// Truthy and Falsy Values 

// 5 falsy values: 0, '', undefined, null, NaN
console.log(Boolean(0));
console.log(Boolean(undefined));
console.log(Boolean('Farzad'));
console.log(Boolean({})); // با اینکه یک آبجکت خالی است اما فالسی ولیو نیست
console.log(Boolean(''));
// type(NaN) = number   خودت نات ا نامبر  یک نامبر است 
const money = 100;
const money; // undefined => 0 => برو تابان 
if (money) {
  console.log(" 100 toman midam ");
} else {
  console.log(' boro Tabane shahr zoodtar '); 
}

let height = 0;
if (height) {
  console.log('YAY! Height is defined');
} else {
  console.log('Height is UNDEFINED');
}

////////////////////////////////////
popop boxes  // سه مدت پاپ آپ داریم
confirm('thi is confirm'); // اگر اوکی کنی ترو میفرسته اگه کنسل کنی فالس
true
confirm('thi is confirm');
false
prompt('chi migi to ?'); // میتونی مقدار از کاربر بگیری اگر مقدار وارد کنه همون مقدار بصورت استرینگ برگشت داده میشه اگه کنسل کنه نال
"hichi"
prompt('chi migi to ?');
null
prompt('chi migi to ?');
"0"
alert('just show me'); // نمایش پیغام
undefined
////////////////////////////////////
// Equality Operators: == vs. ===
// برای کلید کد سعی می کنیم از استریکت استفاده کنیم ===
const age = '18';
if (age === 18) console.log('boro sarbazi :D (strict)'); // تایپ هم چک میکنه

if (age == 18) console.log('boro sarbazi :D (loose)'); // ایج رو که استرینگ هست به عدد تبدیل میکنه
//---------------------------
// تشخیص پرسپولیسی از استفالی
// let color = prompt(' che rangi doos dari?'); 
// undefined
// color
// "red"
// if (color == 'red') {
//     console.log(' ghermezeteeeee afarin ');
// } else if(color == 'blue'){
//     console.log(' nefrine AMOON bar to ');
// } else
//  { console.log(' boro donbale bazit');}
// VM1891:2  ghermezeteeeee afari
//---------------------------

const favourite = Number(prompt("What's your favourite number?"));
console.log(favourite);
console.log(typeof favourite);

if (favourite === 85) { // 
  console.log('Cool! 85 is an amzaing number!')
} else if (favourite === 7) {
  console.log('7 is also a cool number')
} else if (favourite === 9) {
  console.log('9 is also a cool number')
} else {
  console.log('Number is not 85 or 7 or 9')
}

if (favourite !== 85) console.log('Why not 85?');

////////////////////////////////////
// Logical Operators // عملگرهای منطقی - درست و غلطی :)
const hasDriversLicense = true; // A
const hasGoodVision = true; // B

console.log(hasDriversLicense && hasGoodVision);
console.log(hasDriversLicense || hasGoodVision);
console.log(!hasDriversLicense);

// if (hasDriversLicense && hasGoodVision) { // هر دو شرط باید برقرار شه
//   console.log('mahsa is able to drive!');
// } else {
//   console.log('Someone else should drive...');
// }

const isTired = false; // C
console.log(hasDriversLicense && hasGoodVision && isTired);

if (hasDriversLicense && hasGoodVision && !isTired) {
  console.log('mahsa is able to drive!');
} else {
  console.log('Someone else should drive...');
}
*/

/*
////////////////////////////////////
// The switch Statement // شکل دیگری برای پیاده سازی الس ایف های پی در پی 
const day = 'monday';

switch (day) {
    case 'monday': // day === 'monday'
        console.log('دوشنبه است ');
        console.log('صلوات بفرست');
        break;
    case 'tuesday':
        console.log('فیلم ببین');
        break;
    case 'wednesday':
    case 'thursday':
        console.log('نقاشی کن');
        break;
    case 'friday':
        console.log('بخواب');
        break;
    case 'saturday':
    case 'sunday':
        console.log('به سختی کار کن');
        break;
    default:
        console.log('داری اشتباه میزنی ');
}

if (day === 'monday') {
    console.log('دوشنبه است ');
    console.log('صلوات بفرست');
} else if (day === 'tuesday') {
    console.log('فیلم ببین');
} else if (day === 'wednesday' || day === 'thursday') {
    console.log('نقاشی کن')
} else if (day === 'friday') {
    console.log('بخواب');
} else if (day === 'saturday' || day === 'sunday') {
    console.log('سخت کار کن');
} else {
    console.log('داری اشتباه میزنی ');
}

//                دوشنبه است
// script.js: 368 صلوات بفرست
// script.js: 389 دوشنبه است
// script.js: 390 صلوات بفرست
*/

/*
////////////////////////////////////
// Statements and Expressions // => اکسپرشن بخشی از کد است که  یک مقدار را بر می گرداند 
// اکسپرشن داخل استیتمنت استفاده می شود
3 + 4
1995
true && false && !false

if (23 > 10) {
    const str = '23 is bigger';
}

const me = 'Farzad';
console.log(`I'm ${2037 - 1995} years old ${me}`);

////////////////////////////////////
// The Conditional (Ternary) Operator 

// const n = 13 > 6 ? 'greater than ' : 'lower than';
// undefined
// n
// "greater than "

const age = 23;
// age >= 18 ? console.log('I like to drink wine 🍷') : console.log('I like to drink water 💧');

const drink = age >= 18 ? 'wine 🍷' : 'water 💧';
console.log(drink);

let drink2; // اول تعریف میکنیم بعد توی شرط ها مقدار دهی
if (age >= 18) {
    drink2 = 'wine 🍷';
} else {
    drink2 = 'water 💧';
}
console.log(drink2);

console.log(`I like to drink ${age >= 18 ? 'wine 🍷' : 'water 💧'}`); *
/

*/

////////////////////////////////////

/* 
Exception types  // استثنا 
throw expression; // پرتاپ کردن یک اکسپشن

try {
  adddlert("Welcome guest!");
}
catch(err) {
  document.getElementById("demo").innerHTML = err.message;
} 

try {
  Block of code to try
}
catch(err) {
  Block of code to handle errors
}

function myFunction() {
  var message, x;
  message = document.getElementById("p01");
  message.innerHTML = "";
  x = document.getElementById("demo").value;
  try {
    if(x == "") throw "empty";
    if(isNaN(x)) throw "not a number";
    x = Number(x);
    if(x < 5) throw "too low";
    if(x > 10) throw "too high";
  }
  catch(err) {
    message.innerHTML = "Input is " + err;
  }
}

*/
/* 
////////////////////////////////////
// backwards compatible = > یعنی کدهای نوشته شده به زبان جاوا اسکریپت باید برای همیشه قابل اجرا در وب و مدرن جاوا اسکریپت اینجین باشند
// forward compatible = > مدرن جاوا اسکریپت اینجین نمیتونه کدهای آینده رو اجرا کنه

////////////////////////////////////*/