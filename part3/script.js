// === === === === === === === === === === === === === === === === === === ===
//                              Numbers and dates
// === === === === === === === === === === === === === === === === === === ===

////////////////////////
/*
// 
var x = 3.14;    // A number with decimals
var y = 3;       // A number without decimals 

// نوشتن اعداد با حروف علمی
var x = 123e5;    // 12300000
var y = 123e-5;   // 0.00123


// دقت عددی اینتیجر  15 رقم است
var x = 999999999999999;   // x will be 999999999999999
var y = 9999999999999999;  // y will be 10000000000000000


// حداکثر تعداد ارقام دهدهی ۱۷ است، اما محاسبات اعشاری همیشه ۱۰۰٪ دقیق نیست:
var x = 0.2 + 0.1;         // x will be 0.30000000000000004


// عملگر + 

var x = 10;
var y = 20;
var z = x + y; // 30

var x = "10";
var y = "20";
var z = x + y; // "1020" => چسباندن رشته ها به هم

var x = 10;
var y = "20";
var z = x + y; // جمع عدد و رشته یک رشته خواهد بود  "1020"

var x = 10;
var y = 20;
var z = "30";
var result = x + y + z; // 30 + '30' => "3030"


var x = "100";
var y = "10";
var z = x / y; // رشته ها را به عدد تبدیل می کند در عملیات های ریاضی به جز عملگر جمع  

var x = "100";
var y = "10";
var z = x * y; // 1000


var x = "100";
var y = "10";
var z = x - y; // 90


var x = "100";
var y = "10";
var z = x + y;  //"10010"

//NaN : نشون میده که عدد جاری یک عدد قانونی نیست 
var x = 100 / "Apple";  // x will be NaN (Not a Number)

10 + "20"
"1020"
"10" / "7"
1.4285714285714286
"10" / "7a"
NaN 

// isNaN() ; برای تشخیص عدد بودن یک مقدار 
isNaN (4)
false
isNaN('sd')
true



var x = NaN;
var y = 5;
var z = x + y; // NaN => استفاده از نات ا نامبر در عبارات مقدار هم نات ا نامبر می شود

var x = NaN;
var y = "5";
var z = x + y;  // NaN5

typeof NaN  // نوع خود نات ا نامبر یک نامبر است 
"number"


// infinity بینهایت
var myNumber = 2;
while (myNumber != Infinity) {          // تا زمانی که بی نهایت بشه ادامه میده
  myNumber = myNumber * myNumber;
console.log(myNumber);}
VM366:4 4
VM366:4 16
VM366:4 256
VM366:4 65536
VM366:4 4294967296
VM366:4 18446744073709552000
VM366:4 3.402823669209385e+38
VM366:4 1.157920892373162e+77
VM366:4 1.3407807929942597e+154
VM366:4 Infinity 


var x =  2 / 0;          // x will be Infinity تقسیم بر صفر  
var y = -2 / 0;

//   تایپ بی نهایت یک عدد است 
typeof Infinity
"number"

// مبنای 16  => اگر قبل عدد صفر ایکس باشد
var x = 0xFF; // hexa
var x = 067;  // octal

// تبدیل مبنا
var myNumber = 32;
myNumber.toString(10);  // returns 32
myNumber.toString(32);  // returns 10
myNumber.toString(16);  // returns 20
myNumber.toString(8);   // returns 40
myNumber.toString(2);   // returns 100000

let x = 9;
undefined
x
9
x.toString(2)
"1001"
x.toString(16)
"9"
var z = 1458;
undefined
z.toString(16)
"5b2"

// اعداد می توانند آبجکت شوند
var x = 123;
var y = new Number(123);
// typeof x returns number
// typeof y returns object

var x2 = 500;             
var y = new Number(500);
undefined
x2==y
true
x2===y
false 

//----------------------------------------------------------------
//                        methods in numbers
//----------------------------------------------------------------

// toString() : تبدیل عدد به رشته
var x = 123;
x.toString();            // returns 123 from variable x
(123).toString();        // returns 123 from literal 123
(100 + 23).toString();   // returns 123 from expression 100 + 23

//()toExponential  تبدیل عدد به نماد علمی 
var x = 9.656;
x.toExponential(2);     // returns 9.66e+0
x.toExponential(4);     // returns 9.6560e+0
x.toExponential(6);

//()toFixed   یک رشته را با مقدار عددی که با یک عدد دهدهی خاص نوشته شده برمی گرداند

var a = 78.46
undefined
a.toFixed(0)
"78"
a.toFixed(10)
"78.4600000000"
a.toFixed(3)
"78.460"
a.toFixed(2)
"78.46"
a.toFixed(1)
"78.5"
// toFixed(2)  مناسب برای کار با مقادیر پول ست

// toPrecision() رشته بر میگرداند که محتواش یک عدد با یک طول خاص

a.toPrecision()
"78.46"
a.toPrecision(1)
"8e+1"
a.toPrecision(2)
"78"
a.toPrecision(3)
"78.5"
a.toPrecision(4)
"78.46"
a.toPrecision(6)
"78.4600

//متد valueOf)()
همان مقدار عددی را باز می گرداند.
var x = 123;
x.valueOf();            // returns 123 from variable x
(123).valueOf();        // returns 123 from literal 123
(100 + 23).valueOf();   // returns 123 from expression 100 + 23

/------------------------------------
// تبدیل به اعداد

// متد number()

Number(true);          // returns 1
Number(false);         // returns 0
Number("10");          // returns 10
Number("  10");        // returns 10
Number("10  ");        // returns 10
Number(" 10  ");       // returns 10
Number("10.33");       // returns 10.33
Number("10,33");       // returns NaN
Number("10 33");       // returns NaN 
Number("سهیل");        // returns NaN

	
Number(new Date("2017-09-30")); //  مقدار میلی ثانیه از 1970 تا این تاریخ  //1506729600000 

// متد parseInt()  
//یک رشته را تحلیل کرده و یک عدد کامل را به دست می آورد. فضاها مجاز هستند فقط شماره اول بازگردانده می شود: 

parseInt("10");         // returns 10
parseInt("10.33");      // returns 10
parseInt("10 20 30");   // returns 10
parseInt("10 years");   // returns 10
parseInt("years 10");   // returns NaN


// متد parseFloat() 
//یک رشته را تجزیه کرده و یک عدد بازمی گرداند.
// فقط عدد اول بازگردانده می شود و فضاهای خالی نیز مجاز

parseFloat("10");        // returns 10
parseFloat("10.33");     // returns 10.33
parseFloat("10 20 30");  // returns 10
parseFloat("10 years");  // returns 10
parseFloat("years 10");  // returns NaN

Number.MAX_VALUE  // بزرگترین عدد جاواسکریپت
1.7976931348623157e+308
Number.MIN_VALUE  // کوچکترین عدد جاوا اسکریپت
5e-324


// --------------------------------------------
//Math object یک آبجکت برای بحث ریاضی 
// Math
Math {abs: ƒ, acos: ƒ, acosh: ƒ, asin: ƒ, asinh: ƒ, …}
typeof Math
"object"  

// 
Math.round(4.75) // روند کردن به نزدیکترین عدد اینتیجر
5
Math.round(4.45)
4 
// توان
Math.power(20,4)
VM776:1 Uncaught TypeError: Math.power is not a function
    at <anonymous>:1:6
(anonymous) @ VM776:1
Math.pow(20,4)  // توان 
160000

// جذر
  Math.sqrt(128)
11.313708498984761
Math.sqrt(625)
25

// قدر مطلق
Math.abs(-689)
689

// نزدیکترین عدد بالاتر   => سقف
Math.ceil(4.375)
5

// نزدیکترین عدد کوچکتر => کف
Math.floor(4.375)
4

// سینوس بر حسب رادیان
Math.sin(90)
0.8939966636005579

Math.sin(Math.PI / 2)
1 
// 2 * PI = 360  فرمول تبدیل درجه و رادیان 

Math.cos(0 * Math.PI / 180);     // returns 1 (the cos of 0 degrees)


//Math.min و Math.max 
Math.min(0, 150, 30, 20, -8, -200);  // returns -200
Math.max(0, 150, 30, 20, -8, -200);  // returns 150

//Math.random 
Math.floor(Math.random() * 10);     // یک عدد رندوم بین صفر تا 9 

//Math.PI  عدد پی

//Math.E  عدد اویلر 

// ----------------------------------------------
//            Date
// ----------------------------------------------
 // Date()
"Wed Apr 14 2021 12:58:27 GMT+0430 (Iran Daylight Time)"
typeof Date()
"string"
typeof Date
"function" 

// ایجاد شی از دیت
let d = new Date() 
d
Wed Apr 14 2021 13:44:19 GMT+0430 (Iran Daylight Time)
var d = new Date(2018, 11, 24, 10, 33, 30, 0); // var dd =  new Date(2018, 11, 24, 10, 33, 30, 0);
undefined
dd
Mon Dec 24 2018 10: 33: 30 GMT + 0330(Iran Standard Time)


function JSClock() {
  var time = new Date();
  var hour = time.getHours();
  var minute = time.getMinutes();
  var second = time.getSeconds();
  var temp = '' + ((hour > 12) ? hour - 12 : hour);
  if (hour == 0)
    temp = '12';
  temp += ((minute < 10) ? ':0' : ':') + minute;
  temp += ((second < 10) ? ':0' : ':') + second;
  temp += (hour >= 12) ? ' P.M.' : ' A.M.';
  return temp;
}

// === === === === === === === === === === === === === === === === === === ===
//                              Text formatting
// === === === === === === === === === === === === === === === === === === ===

var carname = "pride";  // Double quotes
var carname = 'pride';  // Single quotes

// طول رشته
var txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var sln = txt.length;

// جدا کردن کد در خط بعدی
document.getElementById("demo").innerHTML = "Hello \
Dolly!";

// ساخت آبجکت رشته
var s = 'sahar';
undefined
s
"sahar"
var f = new String('farzad');
undefined
f
String {"farzad"}
typeof f
"object"

// نکته مهم :  اشیا قابل مقایسه نیستند 
var x = new String("hosein");             
var y = new String("hosein");
// (x == y) is false because x and y are different objects

// پیدا کردن مکان یک رشته در رشته ای دیگر
var str = "Please locate where 'locate' occurs!";
var pos = str.indexOf("locate"); // 7

var text = ' man moshtaele eshghe Alieam che konam';
undefined
text.indexOf('ali')
-1
text.indexOf('Ali')
22


// search
var text = ' man moshtaele eshghe Alieam che konam';
text.search('Ali')
22

// slice(start,end)
var text = ' man moshtaele eshghe Alieam che konam';
text.slice(4,12)
" moshtae"

//sunstring()
var str = "Apple, Banana, Kiwi";
var res = str.substring(7, 13);

//substr() = > مثل اسلایس ولی طول رشته به دست آمده رو مشخص میکنه
var str = "Apple, Banana, Kiwi";
var res = str.substr(7, 6); // banana

//replace('kelameei ke mikhad avaz she ', 'kalame jaygozin',) 
	
str = "Please visit snappfood!";
var n = str.replace("snappfood!", "Tabane shahr");  // "Please visit Tabane shahr";


var text1 = "Hello World!";       // String
var text2 = text1.toUpperCase();  // text2 is text1 converted to upper

 var text1 = "Hello World!";       // String
var text2 = text1.toLowerCase();  // text2 is text1 converted to lower

// ادغام 
var text1 = "Hello";
var text2 = "World";
var text3 = text1.concat(" ", text2);


// trim حذف کاراکترهای اضافه اسپیس 
var str = "       Hello World!        ";
alert(str.trim());



خاصيت prototype :
میتونه خواص جدید به یک شی اضافه کنی همانند خواص ذاتی خود شی میشن
Syntax	object.prototype.name = value
* object = نام شی مورد نظر     name = نام خاصيت يا متد جديد     value = مقدار اوليه خاصيت جديد 

// String.prototype متد


if (!String.prototype.trim) {
    String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
};
var str = "       Hello World!        ";
alert(str.trim());

// === === === === === === === === === === === === === === === === === === ===
//                              Regular expressions
// === === === === === === === === === === === === === === === === === === ===
/*


var patt = /taban/i; // regular expression :  /taban/i   عبارت منظم
//taban is pattern  الکو 
// i is modifier  اصلاح کننده

console.log(patt.test('a'));


/reza/i
alireza // true

/reza/i
rez  // false

/reza/i
REZa  // فلگ آی باعث میشه به حروف بزرگ کوچک حساس نباشد  true

/.reza/i  // Find a single character, except newline or line terminator  قبلش باید یک کاراکتر حتما باشد
2REZa  // true

/.reza/i
REZasdfsdf // false

/.reza/i
123456REZasdfsdf // true

/[abc]/i  // Find any character between the brackets  حتما یکی از این کاراکترها باید باشه
farzad // true

/[abc]/i
125j // false

/[a-z]/i  // تعیین کرده بازه
aaa // true

/[a-z]/i
aaa25 // true

/[a-z]/i
25 // false

/[0-9]/i
25 // true 

/[0-90]/i
96 // true

/jpe?g/i // میتونه ای باشه یا نباشه
jpg // true

/jpe?g/i
jpeeg // false

/j{4}/i // حداقل 4 بار تکرار شه
peegjjjj  // true

/j[abc]{4}/i
jabcchhhhhh // true

/a{1,3}/  // Matches any string that contains a sequence of X to Y n's
cndy // false
candy // true
caandy // true
caaaaaaandy //true

/abc$/ // آخرش باید باشد
caaaaaytndyabc // true

/abc[0-9]$/
caaaaaytndyabc2 // true آخرش یک عدد هست

/(a)/ // عینا باشه
aaa // true  بخاطر اولین حرف ای درست شده دو تا ای بعدی مهم نیست

/(ab)/
ababab  // true بخاطر اولین ab
abgggggabab // true

/(ab){3,5}/
abababk // true

/^a/ // ابتدا باید باشد
bc // false
abc // true

/^c#/
c#dfsd // true

var str = "Is this his";
var patt1 = /^Is/g;

/[^0-5]/ // پیدا کردن کاراکتری که بین صفر تا پنج نیست
123456 // true بخاطر شش 

/[^h]/ // به جز اچ ی کاراکتر حداقل باشد
h // false
h2 // چون 2 هست true

// /l4*  میتونه باشه میتونه نباشه هر کدوم از ال و چهار جداگانه بررسی میشه
// Heoool4 World // بخاطر ال چهار true

// /jk*/
/*
// // /jk*/
// // Heooo4 Wor // false
// // Heooo4 Wjor // true
// // Heooo4 Wjkor // true 
/*

/p+/ // حداقل ی ددونه پی باشه
aaaa // false
aaaap // true

/[abc]+/ 
aaaap // حداقل ی دونه ای هست true

\d	Find a digit 
\D	Find a non-digit character
/\d/ // حداقل یک عدد باشه
aaaap4 // true
/\D/  حداقل یک عدد نباشه
aaaap4sdfsd // true

/\w/  // Find a word character
Give 100%! // 
/\w/
سلام // false
/\W/ //Find a non - word character
Give 100%! // true 
/\W/
سلام // true


var re = /\w+\s/g;
var str = 'top top Abbasi';
var myArray = str.match(re);
console.log(myArray);

// ["top ", "top ", "Abbasi "]

// === === === === === === === === === === === === === === === === === === ===
// === === === === === === === === === === === === === === === === === === ===


 */