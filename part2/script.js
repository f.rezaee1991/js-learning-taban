'use strict';

/*
///////////////////////////////////////
// Activating Strict Mode
// استفاده از استریکت مود در اول کد ها 
// در بالای اسکریپت قرار میگیرد در این صورت از قواعد جدید زبان استفاده خواهد شد
//حالت هسته ای جاوا اسکریپت را به حالت مدرن تغییر 
//حالتی است که در آن کدها با شرایط سخت گیرانه تری اجرا می شوند.

let hasDriversLicense = false;
const passTest = true;

if (passTest) hasDriverLicense = true; // چون  اشتباه نوشته شده ارور میده وقتی که از یوز استریکت استفاده بشه
if (hasDriversLicense) console.log('I can drive :D');

// const interface = 'Audio';
// const private = 534;


///////////////////////////////////////
// Functions
function logger() {
  console.log('My name is Farzad');
}

// calling / running / invoking function // => صدا زدن تابع - اجرا کردن 
logger();
logger();
logger();

function salamBacheha() {
    console.log('salam azizane man');
}
salamBacheha(); // salam azizane man
salamBacheha();
salamBacheha();

function sum(x, y) {
    const s = x + y;
    return s;
}
console.log(sum(2, 18)); // 20 


function sum(x, y) {
    console.log(`the sum of ${x} and ${y} equals : ${x + y}`);
}
console.log(sum(2, 18)); // the sum of 2 and 18 equals : 20


const num = Number('23'); // نمونه ای از یک تابع داخلی 


///////////////////////////////////////
// Function Declarations vs. Expressions انواع تعریف فانکشن 

console.log(sayHello());

function sayHello() {
    return 'salam ';
}

const sayBye = function() {
    return 'khoda havez';
}

console.log(sayBye());


// Function declaration => قبل از تعریف فانکشن میتوان آن را صدا زد و بعد تعریف کرد (کد)
function calcAge1(birthYear) {
    return 1400 - birthYear;
}
const age1 = calcAge1(1375);

// Function expression => تا زمانی که تعریف نشده باشد نمیتوان صدا زد 
const calcAge2 = function(birthYear) {
    return 1400 - birthYear;
}
const age2 = calcAge2(1375);

console.log(age1, age2);


///////////////////////////////////////
// Arrow functions  شکل خلاصه ای فانکشن


let power = x => x * x;  // خلاصه تر از بقیه فانکشن ها مستقیم ریترن می کند 
console.log(power(89)); // 89 * 89 = 7021




const calcAge3 = birthYear => 1400 - birthYear;
const age3 = calcAge3(1375);
console.log(age3);

const bazneshastegi = (birthYear, firstName) => { // محاسبه سن بازنشتگی 
    const age = 1400 - birthYear;
    const retirement = 65 - age;
    return `${firstName} retires in ${retirement} years`; // به ریترن نیاز دارد مهم 
}

console.log(bazneshastegi(1375, 'Farzad'));
console.log(bazneshastegi(1381, 'javad'));
// Farzad retires in 40 years
// script.js: 107 javad retires in 46 years


///////////////////////////////////////
// Functions Calling Other Functions صدا زدن تابع درون تابع 


const cap = function(name) {

        return name.toUpperCase();
    }
    // console.log(cap('salam'));

const congradulationMaried = function(name1, name2) {;

    return ` tabrik migam ${cap(name1)} 💑 ${cap(name2)}`;
}

console.log(congradulationMaried('Hamid', 'Hasti')); // tabrik migam HAMID 💑 HASTI


function cutFruitPieces(fruit) {
    return fruit * 4;
}

function fruitProcessor(apples, oranges) {
    const applePieces = cutFruitPieces(apples);
    const orangePieces = cutFruitPieces(oranges);

    const juice = `Juice with ${applePieces} piece of apple and ${orangePieces} pieces of orange.`;
    return juice;
}
console.log(fruitProcessor(2, 3));


///////////////////////////////////////
// Introduction to Arrays   معرفی آرایه ها 
const friend1 = 'javad';
const friend2 = 'fardin';
const friend3 = 'mohammad';

const friends = ['javad','fardin','mohammad'] // روش اول ساخت آرایه
console.log(friends);

const y = new Array(1991, 1984, 2008, 2020); // روش دوم ساخت آرایه 

console.log(friends[0]); // 1991
console.log(friends[2]); // 2008


console.log(friends.length); // طول آرایه 
console.log(friends[friends.length - 1]); // آخرین عنصر آرایه

friends[2] = 'parsa'; // تغییر مقدار یک المان
console.log(friends);

frindes[7] = 'mina'; // اضافه کردن به آرایه در این صورت اندیس هایی قبل از این اندیس هستن خالی میمونن
// چون آرایه پریمیتو نیست حتی بعد از تعریف با کانست تغییر میکنه اما اگه بخوایم آرایه ای دیگه با همین نام تعریف کنیم اجازه نمیده


const firstName = 'Farzad';
const Farzad = [firstName, 'rezaei', 2037 - 1991, 'jigarForoosh', friends];
console.log(Farzad);
console.log(Farzad.length);

// تمرین بیشتر
const calcAge = function (birthYear) {
  return 2037 - birthYear;
}
const years = [1990, 1967, 2002, 2010, 2018];

const age1 = calcAge(years[0]);
const age2 = calcAge(years[1]);
const age3 = calcAge(years[years.length - 1]);
console.log(age1, age2, age3);

const ages = [calcAge(years[0]), calcAge(years[1]), calcAge(years[years.length - 1])];
console.log(ages);


///////////////////////////////////////
// Basic Array Operations (Methods)   متدهای آرایه 
const friends = ['Javad', 'Sohrab', 'Hamed'];

// Add elements  اضافه کردن به آخر آرایه
const newLength = friends.push('Omid'); 
console.log(friends);
console.log(newLength);

friends.unshift('Akbar'); // اضافه کردن به اول آرایه
console.log(friends);

// Remove elements
friends.pop(); // حذف از آخر
const popped = friends.pop();
console.log(popped);
console.log(friends);

friends.shift(); // حذف از اول آرایه
console.log(friends);

console.log(friends.indexOf('Sohrab')); // پیدا کردن شماره اندیس
console.log(friends.indexOf('Akbar'));

friends.push(23);
console.log(friends.includes('Sohrab')); // بررسی اینکه درون آرایه هست یا نه 
console.log(friends.includes('Akbar'));
console.log(friends.includes(23));

if (friends.includes('Sohrab')) {
  console.log('You have a friend called Sohrab');
}
*/

/* 
///////////////////////////////////////
// Introduction to Objects  آبحکت ها 
const FarzadArray = [   //  
    'Farzad',
    'rezaei',
    2037 - 1991,
    'jigarForoosh', ['Javad', 'Hamed', 'Sohrab']
];

const Farzad = {  // آبجکتی که 5 پروپرتی داره 
    firstName: 'Farzad',
    lastName: 'rezaei',
    age: 2037 - 1991,
    job: 'jigarForoosh',
    friends: ['Javad', 'Hamed', 'Sohrab']
};


///////////////////////////////////////
// Dot vs. Bracket Notation     // برای دسترسی به مقادیر آبجکت دو روش هست یکی نقطه یکی هم براکت : در داخل براکت میتونیم اکسپرشن هم داشته باشیم یعنی اینکه عبارتی که ولیو تولید کنه که اون ولیو یک پروپرتی از آبحکت باشه اما در نقطه دسترسی محدودتر است 

const Farzad = {
    firstName: 'Farzad',
    lastName: 'rezaei',
    age: 2037 - 1991,
    job: 'jigarForoosh',
    friends: ['Javad', 'Hamed', 'Sohrab']
};

// Farzad.age
// 46
// Farzad["age"]
// 46
// Farzad["a" + 'ge'] ==> داخل یک اکسپرشن است 
// 46

console.log(Farzad);

console.log(Farzad.lastName);
console.log(Farzad['lastName']);

const nameKey = 'Name';
console.log(Farzad['first' + nameKey]);
console.log(Farzad['last' + nameKey]);

// console.log(Farzad.'last' + nameKey)

const interestedIn = prompt('What do you want to know about Farzad? Choose between firstName, lastName, age, job, and friends');

if (Farzad[interestedIn]) {
    console.log(Farzad[interestedIn]);
} else {
    console.log('Wrong request! Choose between firstName, lastName, age, job, and friends');
}

Farzad.location = 'Iran';
Farzad['twitter'] = '@Farzadsrezaei';
console.log(Farzad);

// Challenge
// "Farzad has 3 friends, and his best friend is called Javad"
console.log(`${Farzad.firstName} has ${Farzad.friends.length} friends, and his best friend is called ${Farzad.friends[0]}`);


///////////////////////////////////////
// Object Methods  متدهای آبجکت

const Farzad = {
    firstName: 'Farzad',
    lastName: 'rezaei',
    birthYear: 1991,
    job: 'jigarForoosh',
    friends: ['Javad', 'Hamed', 'Sohrab'],
    hasDriversLicense: true,

    // calcAge: function (birthYear) {
    //   return 2037 - birthYear;
    // }

    // calcAge: function () {
    //   // console.log(this);
    //   return 2037 - this.birthYear;
    // }

    calcAge: function() {
        this.age = 2037 - this.birthYear;
        return this.age;
    },

    getSummary: function() {
        return `${this.firstName} is a ${this.calcAge()}-year old ${Farzad.job}, and he has ${this.hasDriversLicense ? 'a' : 'no'} driver's license.`
    }
};

// this به شیء ای اشاره می کند که به آن تعلق دارد (در آن قرار دارد).
// اگر this در یک متد استفاده شود، به شیء ای اشاره می کند که صاحب آن متد است (owner object).
// اگر this به تنهایی استفاده شود، به شیء سراسری جاوا اسکریپت (global object) اشاره می کند.
// اگر this در یک تابع استفاده شود، باز هم به شیء سراسری جاوا اسکریپت (global object) اشاره می کند.
// اگر در حالت strict mode باشیم و از this در یک تابع استفاده کنیم، با مقدار undefined مواجه خواهیم شد.
// اگر this در یک event استفاده شود، به عنصری اشاره می کند که آن event را دریافت کرده است.
// متدهایی مانند ()call و ()apply می توانند this را به هر شیء ای برگردانند.


console.log(Farzad.calcAge());

console.log(Farzad.age);
console.log(Farzad.age);
console.log(Farzad.age);


///////////////////////////////////////
// Iteration: The for Loop  => از حلقه فور بیشتر برای شمارش با تعداد معلوم استفاده می کنیم 
// فکر کنید اگه از حلقه ها استفاده نشه باید چقدر کد بنویسیم مثل زیر 

// console.log('Lifting weights repetition 1 🏋️‍♀️');
// console.log('Lifting weights repetition 2 🏋️‍♀️');
// console.log('Lifting weights repetition 3 🏋️‍♀️');
// console.log('Lifting weights repetition 4 🏋️‍♀️');
// console.log('Lifting weights repetition 5 🏋️‍♀️');
// console.log('Lifting weights repetition 6 🏋️‍♀️');
// console.log('Lifting weights repetition 7 🏋️‍♀️');
// console.log('Lifting weights repetition 8 🏋️‍♀️');
// console.log('Lifting weights repetition 9 🏋️‍♀️');
// console.log('Lifting weights repetition 10 🏋️‍♀️');

//  حلقه فور تا زمانی که شرط برقرار باشه کار میکنه
for (let i = 1; i <= 30; i++) {
  console.log(`Lifting weights repetition ${i} 🏋️‍♀️`);
}


///////////////////////////////////////
// Looping Arrays, Breaking and Continuing
const Farzad = [
  'Farzad',
  'rezaei',
  2037 - 1991,
  'jigarForoosh',
  ['Javad', 'Hamed', 'Sohrab'],
  true
];
const types = []; // میخوایم تایپ هر کدام از مقادیر آرایه رو داخلش بریزیم

// console.log(Farzad[0])
// console.log(Farzad[1])
// ...
// console.log(Farzad[4])
// Farzad[5] does NOT exist

for (let i = 0; i < Farzad.length; i++) {
  console.log(Farzad[i], typeof Farzad[i]);
  // Filling types array
  // types[i] = typeof Farzad[i];
  types.push(typeof Farzad[i]);
}

console.log(types);

const years = [1991, 2007, 1969, 2020];
const ages = [];

// for (let i = 0; i < years.length; i++) {
//   ages.push(2037 - years[i]);
// }
// console.log(ages);

// const years = [1991, 2007, 1969, 2020];
// const ages = []
// undefined
// for(let i = 0; i < years.length ; i++) {
//     ages[i] = 2021 - years[i]; }
// 1

// let now = 2021;
// undefined
// for(let i = 0; i < years.length ; i++) {
//     ages[i] = now - years[i]; }
// 1
// ages
// (4) [30, 14, 52, 1]



// continue and break   => کانتینیو  ادامه کد ها رو انجام نمیده و  دوباره به بالای حلقه فور و ادامه حلقه انجام میده - بریک  ادامه حلقه را متوقف میکنه 

console.log('--- ONLY STRINGS ---') // فقط میخوایم رشته ها تولید شن توی مثال بالا 
for (let i = 0; i < Farzad.length; i++) {
  if (typeof Farzad[i] !== 'string') continue;

  console.log(Farzad[i], typeof Farzad[i]);
}

console.log('--- BREAK WITH NUMBER ---')
for (let i = 0; i < Farzad.length; i++) {
  if (typeof Farzad[i] === 'number') break;

  console.log(Farzad[i], typeof Farzad[i]);
}


///////////////////////////////////////
// Looping Backwards and Loops in Loops   پیمایش برعکس و حلقه های تو در تو 
const Farzad = [
  'Farzad',
  'rezaei',
  2037 - 1991,
  'jigarForoosh',
  ['Javad', 'Hamed', 'Sohrab'],
  true
];

// 0, 1, ..., 4
// 4, 3, ..., 0

for (let i = Farzad.length - 1; i >= 0; i--) {
  console.log(i, Farzad[i]);
}

for (let exercise = 1; exercise < 4; exercise++) {
  console.log(`-------- Starting exercise ${exercise}`);

  for (let rep = 1; rep < 6; rep++) {
    console.log(`Exercise ${exercise}: Lifting weight repetition ${rep} 🏋️‍♀️`);
  }
}
 // جدول ضرب با حلقه تو در تو فور 
for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 10; j++) {
        console.log(`${i} * ${j} = ${i * j}`);
    }
    console.log("\n");
}

// 

// VM2069: 2 1 * 0 = 0
// VM2069: 2 1 * 1 = 1
// VM2069: 2 1 * 2 = 2
// VM2069: 2 1 * 3 = 3
// VM2069: 2 1 * 4 = 4
// VM2069: 2 1 * 5 = 5
// VM2069: 2 1 * 6 = 6
// VM2069: 2 1 * 7 = 7
// VM2069: 2 1 * 8 = 8
// VM2069: 2 1 * 9 = 9
// VM2069: 2

// VM2069: 2 2 * 0 = 0
// VM2069: 2 2 * 1 = 2
// VM2069: 2 2 * 2 = 4
// ...


///////////////////////////////////////
// The while Loop    => از حلقه وایل بیشتر برای تکرار با تعداد نا معلوم استفاده می کنیم 

// ساخت جدول ضرب با وایل 
let i = 0;
while (i < 10) {
    let j = 0;
    while (j < 10) {
        console.log(`${i} * ${j} = ${i * j}`);
        j++
    }
    i++
    console.log("\n");
}


for (let rep = 1; rep <= 10; rep++) {
    console.log(`
                Lifting weights repetition $ { rep }🏋️‍♀️
                `);
}

let rep = 1;
while (rep <= 10) {
    // console.log(`
                WHILE: Lifting weights repetition $ { rep }🏋️‍♀️
                `);
    rep++;
}

// ساخت بازی تاس 
// Math.trunc(Math.random() * 6) + 1 => رندوم یک عدد بین صفر و یک میده  ضربدر 6 میکنیم چون 6 تاس داریم  قسمت اعشار رو حذف میکنیم و برای اینکه خود 6 هم باشه یکی اضافه میکنیم

let dice = Math.trunc(Math.random() * 6) + 1;

while (dice !== 6) {
    console.log(`
                You rolled a $ { dice }
                `);
    dice = Math.trunc(Math.random() * 6) + 1;
    if (dice === 6) console.log('Loop is about to end...');
} *
/




///////////////////////////////////////