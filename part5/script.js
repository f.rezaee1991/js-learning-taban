//ادامه oop
// let circle = {
//     radius: 1,
//     location: {
//         x: 1,
//         y: 1

//     },
//     draw: function() {
//         console.log('draw');

//     }
// };
/*

//factory function
function createCircle(radius) {
    return {
        radius,
        draw: function() {
            console.log('draw');
        }
    }
};
const circle = createCircle(8);


// constructor function 

function Circle(radius) {
    this.radius = radius;
    this.draw = function() {
        console.log('draw');
    }
}
const another = new Circle(95);
console.log(another instanceof Circle);

another.location = { x: 6 };
// const another2 = new Circle(105);


// circle { radius: 8, draw: ƒ }
// another
// Circle  { radius: 95, draw: ƒ }
// circle.constructor
// ƒ Object() {
//     [native code] }
// another.constructor
// ƒ Circle(radius) {
//     this.radius = radius;
//     this.draw = function() {
//         console.log('draw');
//     }
// }


// Circle
// ƒ Circle(radius) {
//     this.radius = radius;
//     this.draw = function() {
//         console.log('draw');
//     }
// }
// another
// Circle  { radius: 95, location: {…}, draw: ƒ }
// another.ghiz = true;
// true
// another
// Circle  { radius: 95, location: {…}, ghiz: true, draw: ƒ }
// delet another.ghiz
// VM1510: 1 Uncaught SyntaxError: Unexpected identifier
// delete another.ghiz
// true
// another
// Circle  { radius: 95, location: {…}, draw: ƒ }


// undefined
// for (let key in another) {
//     console.log(key, another[key]);
// }
// VM1867: 2 radius 95
// VM1867: 2 draw ƒ() {
//     console.log('draw');
// }
// VM1867: 2 location { x: 6 }
// undefined
// another
// Circle  { radius: 95, location: {…}, draw: ƒ }
// for (let key in another) {
//     console.log(key, another[key]);
// }
// VM1888: 2 radius 95
// VM1888: 2 draw ƒ() {
//     console.log('draw');
// }
// VM1888: 2 location { x: 6 }
// undefined


// const keysAnother = Object.keys(another);
// console.log(keysAnother); //(3) ["radius", "draw", "location"] 
//===============================================================
// Iterators & Generators
//===============================================================
/*
 تعریف مولد
 // استفاده از *
 // بجای ریترن yield
function* myGenerator() {
    yield 1;
    yield 2;
}


let it = myGenerator();
console.log(it.next());
← {value: 1, done: false}
console.log(it.next());
← {value: 2, done: false}


console.log(it.next());
← {value: undefined, done: true}

// مثال
function* myCounter(){
	let i = 1;
	while(true){
		yield i++;
	}
}

// تعریف iterator
let counter = myCounter();
console.log(counter.next());
← {value: 1, done: false}
console.log(counter.next().value);
← 2
console.log(counter.next().value);
← 3

// مثال سری فیبوناچی

1 , 2 , 3 , 5 , 8 , 13 , 21 , 34 , 55 , 89

function* fibonacci(a , b) {
	let [prev , current] = [a , b];
	while(true) {
		[prev , current] = [current , prev + current];
		yield current;
	}
}


let sequence1 = fibonacci(1 , 2);
let sequence2 = fibonacci(2 , 7);

console.log(sequence1.next().value);
← 3
console.log(sequence1.next().value);
← 5
console.log(sequence1.next().value);
← 8
console.log(sequence1.next().value);
← 13
console.log(sequence1.next().value);
← 21
console.log(sequence2.next().value);
← 9
console.log(sequence2.next().value);
← 16
console.log(sequence2.next().value);
← 25


function* myCounter(start = 1){
	let i = start;
	while(true){
		yield i++;
	}
}


let sequence = fibonacci(0 , 1);
for(let n of sequence){
	console.log(n);
}

 // برای اعداد کوچکتر از 100
let sequence = fibonacci(0 , 1);
for(let n of sequence){
	if(n < 100){
		console.log(n);
	}else{
		break;
	}
}

← 1
← 2
← 3
← 5
← 8
← 13
← 21
← 34
← 55
← 89


//===============================================================
// Promise
//===============================================================
Promise كاربردی مشابه callback function
Asynchronous یا پردازش ناهمگام (غیر همزمان)
synchronous یا پردازش همگام (Blocking)

var promise = new Promise(function(resolve, reject){
     //do something
});

 var promise = new Promise(function(resolve, reject){
     var str="taban.com";
     var site="taban.com";
     if(str===site) {
            resolve();
     } else {
         reject();
     }
 });

 .then(function(result){
        //handle success
 }, function(error){
        //handle error
})

.catch(function(error){
        //handle error
})

promise.then(function () {
     console.log('Promise is successfully.');
}).
 catch(function () {
     console.log('Some error has occured');
});

getting a property on an object:

let handler = {
  get: function(target, name) {
    return name in target? target[name] : 42
  }
}

let p = new Proxy({}, handler)
p.a = 1
console.log(p.a, p.b) // 1, 42

let revocable = Proxy.revocable({}, {
  get: function(target, name) {
    return '[[' + name + ']]'
  }
})
let proxy = revocable.proxy
console.log(proxy.foo)  // "[[foo]]"

revocable.revoke()

console.log(proxy.foo)  // TypeError is thrown
proxy.foo = 1           // TypeError again
delete proxy.foo        // still TypeError
typeof proxy            // "object", typeof doesn't trigger any trap


Reflect.has(Object, 'assign') // true

Function.prototype.apply.call(Math.floor, undefined, [1.75])

Reflect.apply(Math.floor, undefined, [1.75])
// 1

Reflect.apply(String.fromCharCode, undefined, [104, 101, 108, 108, 111])
// "hello"

Reflect.apply(RegExp.prototype.exec, /ab/, ['confabulation']).index
// 4

Reflect.apply(''.charAt, 'ponies', [3])
// "i"

if (Reflect.defineProperty(target, property, attributes)) {
  // success
} else {
  // failure
}


//===============================================================
// Modules
//===============================================================

در فایل user.js :
const name = 
      {
        name: 'Alireza'
      }
export default name


import name from './user.js'

// حالا میتونیم از محتوا استفاده کنیم

// مثال اول
------ file : test.js ------
export function square(x) {
    return x * x;
}
export function diag(x, y) {
    return sqrt(square(x) + square(y));
}

------ file : main.js ------
import { square, diag } from 'test';
console.log(square(11)); // 121
console.log(diag(4, 3)); // 5

// مثال دوم
------ file : myFunc.js ------
export default function () { ... };

------ file : main1.js ------
import myFunc from 'myFunc';
myFunc();


*/